#!/usr/bin/env python2
import gmusicapi as gm
from oauth2client.client import FlowExchangeError
import argparse
import os


class Gim(object):

    supportedExtensions = ['.mp3', '.m4a', '.wma',
                           '.flac', '.ogg', '.m4p']

    def auth(self):
        """Attempts to authenticate a gmusicapi.Musicmanager instance

        If there is no stored oauth, then the user is asked to authenticate.
        Should they fail (or refuse), None is returned.

        """

        mm = gm.Musicmanager()
        gm.Musicmanager.raw_input = lambda _: ""
        if mm.login(self.args.cred):
            return mm
        else:
            try:
                mm.perform_oauth(self.args.cred)
            except FlowExchangeError:
                print("Unable to write-authenticate with the server")
                return None

                if mm.login(self.args.cred):
                    return mm
                else:
                    return None

    @staticmethod
    def parseArgs(args):
        """Parses command line arguments

        Commands should look like:

        gim [--fake] [--cred <path>] <cmd> <path(s)>

        """

        parser = argparse.ArgumentParser(
            description='A command line google music manager')

        parser.add_argument('cmd', nargs='?', help='Which command to run',
                            choices=['push', 'pull', 'auth'])

        parser.add_argument('--fake', '-k', default=False,
                            action='store_true',
                            help='Don\'t actually run the \
                            command, just pretend (use for testing)')

        parser.add_argument('--cred', '-c', default=gm.clients.OAUTH_FILEPATH,
                            help='Change the OAUTH path')

        parser.add_argument('--quiet', '-q', default=False,
                            action='store_true', help='Suppress output')

        parser.add_argument('path', nargs=argparse.REMAINDER,
                            help='The paths of files to upload/download')

        return parser.parse_args(args)

    @staticmethod
    def expandPaths(paths):
        """Expands a dictionary of paths such that it contains only files,
        not directories.

        Follows symlinks.

        """
        newpaths = []
        for path in paths:
            if os.path.isdir(path):
                for root, subFolders, files in os.walk(path, followlinks=True):
                    newpaths += [os.path.join(root, f) for f in files
                                 if os.path.splitext(f)[1]
                                 in Gim.supportedExtensions]
            elif os.path.splitext(path)[1] in Gim.supportedExtensions:
                newpaths.append(path)

        return newpaths

    def push(self):
        """Uploads music to GMusic

        If fake = True, then the upload is not actually performed.

        Returns the output from gmusicapi.upload.

        If fake = True, then an output is returned which presumes each song
        uploaded successfully.

        If authentification fails, then an output is returned which presumes
        each song failed to upload.

        Authentification failure takes priority over fakeness.

        """

        if self.mm is None:
            return (
                {},
                {},
                {path: 'AUTH_FAILED' for path in self.paths}
            )

        if self.args.fake is True:
            return (
                {path: '-1' for path in self.paths},
                {},
                {},
            )

        return self.mm.upload(self.paths)

    @staticmethod
    def prettifyResults(results):
        """Makes some pretty output from push results"""

        pretty = ""
        for match, songid in results[1].iteritems():
            pretty += "Matched: {0}\n".format(match)
        for failed, reason in results[2].iteritems():
            pretty += "Failed: {0} ({1})\n".format(failed, reason)

        pretty += "Uploaded: {0}\nMatched: {1}\nFailed: {2}".format(
            len(results[0]), len(results[1]), len(results[2])
        )

        return pretty

    def run(self):
        if self.args.cmd == "auth":
            self.auth()
        elif self.args.cmd == "push":
            print(Gim.prettifyResults(self.push()))

    def __init__(self, args=None, auth=True, paths=[]):
        self.args = Gim.parseArgs(args)
        self.paths = self.expandPaths(self.args.path + paths)
        if auth:
            self.mm = self.auth()
        else:
            self.mm = None

if __name__ == '__main__':
    gim = Gim()
    gim.run()
