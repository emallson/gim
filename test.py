#!/usr/bin/env python2
import unittest
from gim import Gim


class GimAuth(unittest.TestCase):

    def setUp(self):
        self.gim = Gim(args=[], auth=False)

    def test_auth(self):
        self.assertNotEqual(self.gim.auth(), None)


class GimArgs(unittest.TestCase):

    def setUp(self):
        self.path = 'test_dir/foo.mp3'
        self.authArgs = ['auth']
        self.pushArgs = ['push', self.path]
        self.fakeArgs = ['--fake', 'push', self.path]

    def test_argparse(self):
        result = Gim.parseArgs(self.authArgs)
        self.assertEqual(result.cmd, 'auth')

        result = Gim.parseArgs(self.pushArgs)
        self.assertEqual(result.cmd, 'push')
        self.assertEqual(result.path, [self.path])

        result = Gim.parseArgs(self.fakeArgs)
        self.assertEqual(result.cmd, 'push')
        self.assertEqual(result.path, [self.path])
        self.assertTrue(result.fake)


class GimPush(unittest.TestCase):

    def setUp(self):
        self.paths = [
            'test_dir',
        ]

        self.fakeResult = (
            {'test_dir/foo.mp3': '-1', 'test_dir/blah/bar.ogg': '-1'},
            {},
            {}
        )

        self.unauthResult = (
            {},
            {},
            {
                'test_dir/foo.mp3': 'AUTH_FAILED',
                'test_dir/blah/bar.ogg': 'AUTH_FAILED'
            }
        )

    def test_push(self):
        gim = Gim(args=['--fake'], paths=self.paths)
        self.assertEquals(gim.push(),
                          self.fakeResult)
        gim = Gim(auth=False, paths=self.paths)
        self.assertEquals(gim.push(), self.unauthResult)


class GimExpandPaths(unittest.TestCase):

    def setUp(self):
        self.paths = [
            'test_dir'
        ]

        self.expPaths = [
            'test_dir/foo.mp3',
            'test_dir/blah/bar.ogg'
        ]

    def test_expand(self):
        self.assertEquals(Gim.expandPaths(self.paths), self.expPaths)


class GimPrettyPaths(unittest.TestCase):

    def setUp(self):
        self.fakeResult = (
            {'foo': '-1', 'bar': '-1', 'blah': '-1'},
            {},
            {}
        )

        self.prettyFakeResult = "Uploaded: 3\nMatched: 0\nFailed: 0"

        self.unauthResult = (
            {},
            {},
            {
                'foo': 'AUTH_FAILED',
                'bar': 'AUTH_FAILED',
                'blah': 'AUTH_FAILED'
            }
        )

        self.prettyUnauthResult = "Failed: blah (AUTH_FAILED)\nFailed: foo (AUTH_FAILED)\nFailed: bar (AUTH_FAILED)\nUploaded: 0\nMatched: 0\nFailed: 3"

        self.matchResult = (
            {},
            {'foo': '-1'},
            {}
        )

        self.prettyMatchResult = "Matched: foo\nUploaded: 0\nMatched: 1\nFailed: 0"

    def test_pretty(self):
        self.assertEqual(Gim.prettifyResults(self.fakeResult),
                         self.prettyFakeResult)
        self.assertEqual(Gim.prettifyResults(self.unauthResult),
                         self.prettyUnauthResult)
        self.assertEqual(Gim.prettifyResults(self.matchResult),
                         self.prettyMatchResult)

if __name__ == '__main__':
    unittest.main()
